package com.example.haley_chen.watchout;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class Mainmenu extends ActionBarActivity {
    private String phone_number = "1-408-823-2556";
    private String message = "What BE YONDER?";
    private int interval = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainmenu);

        // Customize action bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customActionBar = inflator.inflate(R.layout.action_bar, null);
        actionBar.setCustomView(customActionBar);

//        ImageButton make_call_btn = (ImageButton) findViewById(R.id.emergency_call);
//        make_call_btn.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                makeCall(phone_number);
//            }
//        });

        ImageButton safety_checks_btn = (ImageButton) findViewById(R.id.safety_checks);
        safety_checks_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent safety_checks_intent = new Intent(getApplicationContext(), Quick_checks_menu.class);
                startActivity(safety_checks_intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void makeCall(String phone_number) {
        Intent phoneIntent = new Intent(Intent.ACTION_CALL);
        phoneIntent.setData(Uri.parse("tel:" + phone_number));

        try {
            startActivity(phoneIntent);
            finish();
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(Mainmenu.this,
                    "Call failed. Please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
    }

}
