package com.example.haley_chen.watchout;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.GregorianCalendar;

/**
 * Created by haley_chen on 12/11/14.
 */
public class Quick_checks_menu extends ActionBarActivity{
    private String phone_number = "1-408-320-5987";
//    private String message = "What BE YONDER?";
    // Test interval. Change in handleTimeIntervalButton() function for real functionality.
    private int interval = 10000;
    private String intervalString;
    private EditText message;
    private boolean recipient1ButtonState = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_notification);

        createActionBar();

        message = (EditText) findViewById(R.id.message);

        // Choose Message button
        chooseMessageButton();

        // Set Time Interval
        handleTimeIntervalButton();

        // Set Recipients
        handleRecipientsButton();

        // Begin Safety Checks button
        beginSafetyChecksButton();
    }
    private void handleRecipientsButton() {
        ImageButton selectRecipientsButton = (ImageButton) findViewById(R.id.select_recipients_btn);

        // Update selected recipients based on Toggles and new entries
        final TextView person2 = (TextView) findViewById(R.id.person2);
        final ImageView person2Image = (ImageView) findViewById(R.id.person2_image);
        final TableRow lindseyStirlingRow = (TableRow) findViewById(R.id.lindsey_stirling_info);

        selectRecipientsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                final View recipientsDialogView = inflater.inflate(R.layout.recipients_dialog, null);
                final View alarmNotificationView = inflater.inflate(R.layout.alarm_notification, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Quick_checks_menu.this);
                alertDialogBuilder.setView(recipientsDialogView);

                toggleSelectedRecipients(recipientsDialogView);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        EditText name_editText = (EditText) recipientsDialogView.findViewById(R.id.add_name);
                                        EditText phone_number_editText = (EditText) recipientsDialogView.findViewById(R.id.add_phone_number);

                                        if (phone_number_editText != null && phone_number_editText.length() > 0
                                                && person2 != null && person2.getText().length() > 0) {
                                            phone_number = phone_number_editText.getText().toString();
                                            Log.v("Phone number", phone_number);
                                            Log.v("not null", "person2");
                                            person2.setText(name_editText.getText().toString());
                                            person2.setVisibility(View.VISIBLE);
                                            person2Image.setVisibility(View.VISIBLE);
                                        }
                                        else {
                                            Log.v("person2", "Invisible");
                                            person2Image.setVisibility(View.INVISIBLE);
                                            person2.setVisibility(View.INVISIBLE);
                                        }

                                        if (recipient1ButtonState) {
                                            Log.v("setting to visible", "visible");
                                            for (int i = 0; i < lindseyStirlingRow.getChildCount(); i++) {
                                                Log.v("lindseystirlingrow", "" + i);
                                                lindseyStirlingRow.getChildAt(i).setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            Log.v("setting to invisible", "invisible");
                                            for (int i = 0; i < lindseyStirlingRow.getChildCount(); i++) {
                                                lindseyStirlingRow.getChildAt(i).setVisibility(View.INVISIBLE);
                                            }
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private void toggleSelectedRecipients(View view) {
        final ImageButton recipient1Selected = (ImageButton) view.findViewById(R.id.select_recipient1);

        // Initial view
        if (recipient1ButtonState) {
            recipient1Selected.setImageResource(R.drawable.selected_recipient);
        }
        else {
            recipient1Selected.setImageResource(R.drawable.deselected_recipient);
        }

        recipient1Selected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (recipient1ButtonState) {
                    recipient1Selected.setImageResource(R.drawable.deselected_recipient);
                    recipient1ButtonState = false;
                }
                else {
                    recipient1Selected.setImageResource(R.drawable.selected_recipient);
                    recipient1ButtonState = true;
                }
            }
        });

    }

    private void handleTimeIntervalButton() {
        ImageButton set_time_interval_button = (ImageButton) findViewById(R.id.choose_interval_btn);
        set_time_interval_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                View setIntervalDialogView = inflater.inflate(R.layout.set_interval_dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Quick_checks_menu.this);
                alertDialogBuilder.setView(setIntervalDialogView);

                final TextView selectedTimeIntervalString = (TextView) findViewById(R.id.interval);
                final TimePicker timePicker = (TimePicker) setIntervalDialogView.findViewById(R.id.timePicker);
                timePicker.setIs24HourView(true);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(true)
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setPositiveButton("Set time",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        int hour = timePicker.getCurrentHour();
                                        int minute = timePicker.getCurrentMinute();

                                        // Uncomment this if we want real functionality.
//                                        interval = (1000) * minute + (1000) * 60 * hour;
                                        String text = hour + "";
                                        if (hour <= 1) {
                                            text = text + " hour " + minute;
                                        }
                                        else {
                                            text += text + " hours " + minute;
                                        }
                                        if (minute <= 1) {
                                            text += " minute";
                                        }
                                        else {
                                            text += " minutes.";
                                        }
                                        selectedTimeIntervalString.setText(text);

                                        // get user input and set it to result
                                        // edit text
//                                        message.setText(messageInput.getText());
                                    }
                                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private void beginSafetyChecksButton() {
        ImageButton start_safety_checks_button = (ImageButton) findViewById(R.id.begin_safety_checks_button);
        start_safety_checks_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent return_to_home_intent = new Intent(getApplicationContext(), Mainmenu.class);
                startActivity(return_to_home_intent);
                Log.v("Sending text to", phone_number);
                sendText(phone_number, message.getText().toString(), interval);

                Toast.makeText(getApplicationContext(), "Quick Checks initiated.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void chooseMessageButton(){
        ImageButton set_message_button = (ImageButton) findViewById(R.id.choose_message_button);
        set_message_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                View messagePromptView = inflater.inflate(R.layout.message_input_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Quick_checks_menu.this);
                alertDialogBuilder.setView(messagePromptView);

                final EditText messageInput = (EditText) messagePromptView.findViewById(R.id.message_input);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        message.setText(messageInput.getText());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    void createActionBar() {
        // Customize action bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("QuickChecks");
    }

    /*
     * @interval (int) in milliseconds
     */
    public void sendText(final String phone_number, String message, int interval) {
        final String myMessage = message;
        final int myInterval = interval;

        int STUB_END_CONDITION = 3;
        for (int count = 1; count <= STUB_END_CONDITION; count++) {
            Long time = new GregorianCalendar().getTimeInMillis() + interval * count;
            Intent intentAlarm = new Intent(Quick_checks_menu.this, AlarmReceiver.class);

            Bundle bundle = new Bundle();
            intentAlarm.putExtra("message", myMessage);
            intentAlarm.putExtra("phone_number", phone_number);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(Quick_checks_menu.this, count,
                    intentAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            // Set the alarm for a particular time
//            Log.v("AlarmSet", "Setting alarm for time: " + time.toString());
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        }

//        Thread thread = new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                try {
//                    int STUB_END_CONDITION = 5;
//                    for (int count = 1; count <= STUB_END_CONDITION; count++) {
//                        SmsManager smsManager = SmsManager.getDefault();
//                        smsManager.sendTextMessage(phone_number, null,
//                                myMessage, null, null);
//                        Log.v("Thread sleeping", "" + myInterval);
//                        Thread.sleep(myInterval);
////                        new CountDownTimer(myInterval, 1000) {
////                            public void onTick(long millisUntilFinished) {}
////                            public void onFinish() {}
////                        }.start();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        thread.start();
    }
}
