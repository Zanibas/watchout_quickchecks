package com.example.haley_chen.watchout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;

/**
 * Created by haley_chen on 12/11/14.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("Received", "onReceive");

        Bundle bundle = intent.getExtras();
        String message = (String)bundle.getCharSequence("message");
        String phone_number = (String)bundle.getCharSequence("phone_number");
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phone_number, null,
                message, null, null);
    }
}
